import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class Main {

    public static void main(String[] args) {

        int day = 31;
        int month = 12;
        int year = 1990;

        System.out.println(collectBirthdays(year, month, day));

    }

    public static String collectBirthdays(int year, int month, int day) {
        Calendar calendar = new GregorianCalendar(year, month-1, day);
        Calendar calendar1 = Calendar.getInstance();
        Date inputDate = calendar.getTime();
        Date currentDate = calendar1.getTime();
        StringBuilder stringBuilder = new StringBuilder();
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy - EEE", Locale.US);
        int currentYear = calendar1.get(Calendar.YEAR);
        stringBuilder.append("0")
                .append(" - ")
                .append(dateFormat.format(inputDate))
                .append(System.lineSeparator());
        for (int i = 1, j = year; i < currentYear; i++, j++) {
            if (inputDate.after(currentDate)) {
                return "";
            }
            if (inputDate.before(currentDate)) {
                calendar.add(Calendar.YEAR, 1);
                inputDate = calendar.getTime();
                if (inputDate.after(currentDate)) {
                    return stringBuilder.toString();
                }
            }
            calendar.add(Calendar.YEAR, 1);
            inputDate = calendar.getTime();
            stringBuilder.append(i)
                    .append(" - ")
                    .append(dateFormat.format(inputDate))
                    .append(System.lineSeparator());
        }
        return "";
    }
}
