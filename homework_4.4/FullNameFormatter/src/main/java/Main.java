import java.util.Scanner;

public class Main {

  public static void main(String[] args) {

    Scanner scanner = new Scanner(System.in);
    while (true) {
      String input = scanner.nextLine();
      if (input.equals("0")) {
        break;
      }
      printFullName(input);
      //TODO:напишите ваш код тут, результат вывести в консоль.
      //При невалидном ФИО вывести в консоль: Введенная строка не является ФИО
    }
  }
  public static void printFullName(String fullName){
    String out="Введенная строка не является ФИО";
    String lastName="", firstName="", middleName="";
    int countSpace=0;
    if(isValidFullName(fullName)){
      for(char ch: fullName.toCharArray()){
        if(ch==' '){
          countSpace++;
        }else{
          switch(countSpace){
            case 0:
              lastName+=ch;
              break;
            case 1:
              firstName+=ch;
              break;
            case 2:
              middleName+=ch;
              break;
          }
        }
      }
      out="Фамилия: "+ lastName + "\nИмя: "+ firstName+"\nОтчество: "+ middleName;
    }
    System.out.println(out);
  }
  private static boolean isValidFullName(String fullName){
    boolean isValid=true;
    int countSpace=0;
    for(char ch: fullName.toCharArray()){
      Scanner scanner=new Scanner( Character.toString(ch));
      if(ch==' '){
        countSpace++;
      }
      if(scanner.hasNextInt()){
        isValid=false;
        break;
      }
      if(countSpace>2){
        isValid=false;
        break;
      }
    }
    if(countSpace!=2){
      isValid=false;
    }
    return isValid;
  }
}


