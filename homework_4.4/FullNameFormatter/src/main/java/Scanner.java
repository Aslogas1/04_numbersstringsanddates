public class Scanner {
    private String nameStr;
    private Scanner(String name){
        nameStr = name;
        printName();
    }
    public void printName(){
        String[] words = nameStr.split(" ");
        if(words.length !=3) {
            System.out.println("Вы ввели некорректные данные");
        }else {
            System.out.println("Фамилия: " + words[0]);
            System.out.println("Имя: " + words[1]);
            System.out.println("Отчество: " + words[2]);
        }
    }
}
