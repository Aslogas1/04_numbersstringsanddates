public class Main {

  public static void main(String[] args) {
    String text = "Вася заработал 5000 рублей, Петя - 7563 рубля, а Маша - 30000 рублей";
    //TODO: напишите ваш код, результат вывести в консоль
    int ac = 0, sum = 0;
    int l, i;
    char c;
    l = text.length();
    for (i = 0; i < l; i++) {
      c = text.charAt(i);
      if (c >= '0' && c <= '9')
        ac = ac * 10 + (c - '0');
      else {
        if (ac > 0) sum += ac;
        ac = 0;
      }
    }
    System.out.println(sum);
  }
}