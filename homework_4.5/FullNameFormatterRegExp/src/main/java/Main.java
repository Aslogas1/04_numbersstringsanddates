import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    while (true) {
      String input =  scanner.nextLine();
      if (input.equals("0")) {
        break;
      }
      String regexp = "(^([А-Яа-я\\-]+)\\s+([А-Яа-я]+)\\s+([А-Яа-я]+)$)";
      if (input.matches(regexp)) {
        Pattern pattern = Pattern.compile(regexp);
        Matcher matcher = pattern.matcher(input);
        matcher.find();
        System.out.println("Фамилия: " + matcher.group(2));
        System.out.println("Имя: " + matcher.group(3));
        System.out.println("Отчество: " + matcher.group(4));
      } else {
        System.out.println("Введенная строка не является ФИО");
      }
      // TODO:напишите ваш код тут, результат вывести в консоль.
      // При невалидном ФИО вывести в консоль: Введенная строка не является ФИО
    }
  }

}