public class Main {

  public static void main(String[] args) {
    Container container = new Container();
    container.count += 7843;
  }

  /* Реализуйте метод sumDigits который возвращает сумму цифр числа, пример:
  передано 12345, метод должен вернуть 15
  если передано null, то должен вернуть -1

  Запустите тесты TestSumDigits для проверки корректности работы метода

  не меняйте название метода, его возвращаемое значение и модификаторы доступа (public).
  В противном случае тестовый метод не сможет проверить ваш код
   */

  public static int sumDigits(Integer n)
  {
    if (n == null){
      return -1;
    }
    int sum = 0;
    String num = Integer.toString(n);
    for (char e: num.toCharArray()){
      sum += Character.getNumericValue(e);
    }
    return sum;
  }
}
